package com.team.gemastik_mealplanner.ui.viewmodel;

import android.widget.CheckBox;

import com.team.gemastik_mealplanner.data.model.DietType;
import com.team.gemastik_mealplanner.data.model.PreRegisteredUser;

import java.math.BigInteger;
import java.util.ArrayList;

public class FoodScreeningViewModel {

    public void setPriceLimit(int position, PreRegisteredUser preRegisteredUser){
        BigInteger[] priceLimit = new BigInteger[2];
        switch (position){
            case 0:
                priceLimit[0] = BigInteger.valueOf(0);
                priceLimit[1] = BigInteger.valueOf(0);
                break;
            case 1:
                priceLimit[0] = BigInteger.valueOf(15000);
                priceLimit[1] = BigInteger.valueOf(25000);
                break;
            case 2:
                priceLimit[0] = BigInteger.valueOf(26000);
                priceLimit[1] = BigInteger.valueOf(35000);
                break;
            case 3:
                priceLimit[0] = BigInteger.valueOf(36000);
                priceLimit[1] = BigInteger.valueOf(45000);
                break;
            case 4:
                priceLimit[0] = BigInteger.valueOf(46000);
                priceLimit[1] = BigInteger.valueOf(55000);
                break;
            case 5:
                priceLimit[0] = BigInteger.valueOf(56000);
                priceLimit[1] = BigInteger.valueOf(60000);
                break;
            case 6:
                priceLimit[0] = BigInteger.valueOf(60000);
                priceLimit[1] = BigInteger.valueOf(100000);
                break;
        }
        preRegisteredUser.setPriceLimit(priceLimit);
    }

    public void foodTypeSelected(String dietName,CheckBox[] checkBoxes,PreRegisteredUser preRegisteredUser){
        switch(dietName){
            case "Apa saja":
                preRegisteredUser.getDietType().setDietName("anything");
                preRegisteredUser.getDietType().setUserAllergy(DietType.anything);
                break;
            case "Paleo":
                preRegisteredUser.getDietType().setDietName("paleo");
                preRegisteredUser.getDietType().setUserAllergy(DietType.paleo);
                break;
            case "Vegetarian":
                preRegisteredUser.getDietType().setDietName("vegetarian");
                preRegisteredUser.getDietType().setUserAllergy(DietType.vegetarian);
                break;
            case "Vegan":
                preRegisteredUser.getDietType().setDietName("vegan");
                preRegisteredUser.getDietType().setUserAllergy(DietType.vegan);
                break;
            case "Ketogenik":
                preRegisteredUser.getDietType().setDietName("ketogenik");
                preRegisteredUser.getDietType().setUserAllergy(DietType.ketogenik);
                break;
            case "Mediterania":
                preRegisteredUser.getDietType().setDietName("mediterania");
                preRegisteredUser.getDietType().setUserAllergy(DietType.mediterania);
                break;
        }

        for (CheckBox checkboxes:
             checkBoxes) {
            if (preRegisteredUser.getDietType().getUserAllergy().contains(String.valueOf(checkboxes.getText()))){
                checkboxes.setChecked(true);
            }else{
                checkboxes.setChecked(false);
            }
        }

    }

    public void addAvoid(String ingredient,PreRegisteredUser preRegisteredUser){
        preRegisteredUser.getDietType().getUserAllergy().add(ingredient);
    }
    public void removeAvoid(String ingredient,PreRegisteredUser preRegisteredUser){
        preRegisteredUser.getDietType().getUserAllergy().remove(ingredient);
    }

}
